# Notes
Adjunto encontrarán la especificación del Proyecto Final, ver archivo Word. Para llevarlo acabo deben formar equipos, máximo tres personas.

Los detalles de entrega y evaluación los veremos en clase. Para apoyarlos en sus avances, les propongo las siguientes fechas de revisión:
- 15 de noviembre: Esquema conceptual de la BD usando ER o UML.
- 22 de noviembre: Descripción de las funciones del sistema (especificación de las transacciones).
- 29 de noviembre: Arquitectura a dos tercios, describir las funciones agrupadas por modulos.

La fecha de entrega está tentativamente programada para el viernes 4 de diciembre.

# Final Project

## Objective
Travel agency '_PONCHITO_' needs to computerize their process of 'trip reservation' using a relational DBMS.
The agency already has an automated brochure that allows the user to simulate a reservation.
The brochure is presented as a set of relations that group information on cities, circuits, monuments, museums, hotels, etc.
It is also necessary to define relations for authorizing a potential client of the agency, complete a simulation of the trip, and later go to the agency to validate the options of the simulation.

The application must provide functions that allow for the consultation, modification, and addition of information to this database. It must, for example, be able to accept new clients and authorize a trip simulation after validation.

## Defining the database schema
The information in this application must be managed by a relational database. A part of the relational schema for this database will be predefined. You must not modify the relational schema provided, but you may add new relations to represent the additional information mecessary for the implementation of the application.

### _Provided Schema_
The provided schema corresponds to the trip catalog (brochure/folleto).
This part contains information on the possible trips offered by the agency '_PONCHITO_'.
The relations that describe the cities and countries offered by the agency, as well as the places of interest (monumets, museums, etc.), circuits, and hotels are defined as follows:
```
Ciudad( nombre char(12), país char(12) )

Una ciudad tiene un nombre y pertenece a un país. Se supone que no hay dos ciudades con el mismo nombre en un país dado.

LugarAvisitar(	nombre char(12), ciudad char(12), país char(12), dirección char(20),
	descripción char(40), precio int   )

Un lugar (un sitio, un monumento, un museo) a visitar tiene un nombre y una descripción. Se encuentra en una dirección en una ciudad de un país y su visita cuesta cierto precio. En el caso de algunos lugares el precio puede ser igual a 0.

Circuito(	identificador char(5), descripción char(20), ciudadSalida char(12),
	paísSalida char(12), ciudadLlegada char(12), paísLlegada char(12),
	duración int, precio int   )

Un circuito tiene un identificador y una descripción. Se realiza entre una ciudad de salida (en un país de salida) y una ciudad de llegada (en un país de llegada), tiene una duración y cuesta un precio.

FechaCircuito( identificador char(5), fechaSalida date, nbPersonas int )

Un mismo circuito puede ser propuesto en fechas diferentes (fechaSalida) para un cierto número de personas.

Etapa(	identificador char(5), orden int, nombreLugar char(12), ciudad char(12),
país char(12), duración int )

Un circuito corresponde a una lista de lugares a visitar (etapas) en cierto orden. Cada etapa hace referencia al lugar que se visita en una ciudad y tiene una duración.

Hotel(	nombre char(12), ciudad char(12), país char(12), dirección char(20), numCuartos int,
precioCuarto int, precioDesayuno int )

Un hotel está situado en una dirección en una ciudad y tiene numCuartos cuartos con un costo precioCuarto. Propone un desayuno a un precio precioDesayuno.
```

### _Schema to define_
With the information you have, the agency '_PONCHITO_' can propose different trip simulations to their clients. A simulation includes:
* the name of the person (known client or potential) that requests the simulation,
* the departure and arrival dates of the trip,
* the number of people participating in the trip,
* the different reservation options taken by the client after examining the suggested information, i.e., the country considered for the trip, the cities to be visited, places, circuits, and selected hotels.
* (calculated) cost of the trip.

After validating a simulation, the user receives a simulation number with which they may present at the agency in order to complete the trip reservation.
To complete the reservation, it is necessary to validate the information of the simulation (check if the dates and reservation options are always valid), and complete the reservations with the corresponding organizations.

The agency administers in its database the reservations of each client. In order to authorize a reservation, it is necessary to register the client in the database, if they are not already registered.
A client is registered with thier name, a type (company, group, individual), and year of registration.
In the group of clients, there must not exist a client without a reservation.
A client may be an employee of the agency ( user of the category 'agencia'), and as such, benefits from an employee discount on their trip.

It is important to note that, after a couple of days, a simulation will no longer be considered valid. The information related to the simulation stored in the database is eliminated.

To complete the relational schema given in section 2.1, you must create the necessary relations to administrate the information pertaining to the simulations and reservations.


## Application Functionality
The application must provide the necessary functions for the management of the simulations and reservations as well as administration.
The following are the esential functions.

### _Brochure inquiry (Consulta de folleto)_
You must develop (a) program(s) for the application '_PONCHITO_' that allow the retrieval of information about trips suggested by the agency.

You must allow n authorized user (verified access rights) obtain the information about the countries to visit, the cities en those countries, the circuits in those cities, the hotels in those cities, etc.

The primary functions of this program are the following:
* Allow a user to select the type of information they would like to see.
* The program asks the user to specify the required information to consult with the database. Initially it will be limited to queries expressed as text.

### _Simulate a trip reservation_
The reservation program allows a user to confirm a trip simulation. The primary functions are as follows:
* Capture the clients information: If the person that presents themselves to the agency is not a known client, the program must then ask for additional information from the client( their name, address, payment type, etc.). In the case that the person is a known client, the program must make sure that the stored information on that client is up to date.
* Create a new trip reservation based on a simulation: se trata de la lista de reservaciones de circuitos, de lugares a visitar, de hoteles, de tours, etc. para validar cada opción. Por ejemplo, en el caso de indisponibilidad de ciertos hoteles o circuitos (falta de cuartos, exceso de participantes, etc.) el cliente tendrá que elegir otras opciones o cancelar todo. De manera general, es importante asegurar la coherencia de la información de la reservación.
* Evaluate the cost of a trip and ask for the necessary information to complete the payment.
* Present the list of reservations.
* Validate a reservation, the application most return a reservation number.

### _Interfaces (optional)_
For the '_PONCHITO_' application, you may design three distinct interfaces, each related to a type of user:
* Public interface: allows a public user (unknown) to ask for the information related to the trips suggested by the agency and simulate a trip.
* Client interface: allows a client user (known) to check their reservations. A client of the agency has at least one reservation with the agency.
* Agency interface: allows an agency worker to:
    1. Consult the brochure and resrvations of a client.
    2. Create clients.
    3. Complete trip reservations (simulate and/or validate reservations).
    The agency worker may also modify already validated reservations.
* Global interface: groups the interfaces described above. In orderto utilize the functions of the application, the user must enter the sistem as a public user, client, or worker. By default, and user can access the public interface. However, only known users may access the client interface, and as such, users will have to enter the application as a client. Similarly, for a user to enter the Agencyinterface, they must enter as an agency worker and be registered in the corresponding category. NOte that a password must be assigned to the users at the time of their creation.


## Conclusions
The application must maintain coherency in the database. For this, design transactions and when possible, use the services provided by the DBMS, to maintain coherency. (using integrity restrictions).
For the development of your application, you must prioritize the tasks related to the administration of the database. You have full liberty with respect to the user interface, but these _**are not the central point of the project**_. A good interface will receive extra credit only if ll other aspects of the database are completed.
On the other hand, you must be rigorous in designing the database schema:
* build an entity-relation diagram
* transform this diagram into a realtional schema in 3FN
* specify the integrity restrictions (domain restrictions, primary keys, foreign keys) and implement them.

## Evaluacion
A demonstration of your implementation will take place at the end of the course. For this, you must write a report that includes:
* your model
* the database schema created
* the implementation, as well as the transactions used.

The report must be turned in before you present. Consider the definition of a group of tests for the demonstration.
