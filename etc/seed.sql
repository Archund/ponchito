#
# TABLE STRUCTURE FOR: Circuito
#

DROP TABLE IF EXISTS Circuito;

CREATE TABLE Circuito (
  identificador char(5) COLLATE utf8_unicode_ci NOT NULL,
  descripcion char(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  ciudadSalida char(12) COLLATE utf8_unicode_ci DEFAULT NULL,
  paisSalida char(12) COLLATE utf8_unicode_ci DEFAULT NULL,
  ciudadLlegada char(12) COLLATE utf8_unicode_ci DEFAULT NULL,
  paisLlegada char(12) COLLATE utf8_unicode_ci DEFAULT NULL,
  duracion int DEFAULT NULL,
  precio int DEFAULT NULL,
  PRIMARY KEY (identificador)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO Circuito (identificador, descripcion, ciudadSalida, paisSalida, ciudadLlegada, paisLlegada, duracion, precio) VALUES ('A.', 'Soluta est.', 'Charlieport', 'CN', 'Charlieport', 'CN', 7, 9);
INSERT INTO Circuito (identificador, descripcion, ciudadSalida, paisSalida, ciudadLlegada, paisLlegada, duracion, precio) VALUES ('Aut.', 'Dicta.', 'Port Alessan', 'PT', 'Port Alessan', 'PT', 1, 9);
INSERT INTO Circuito (identificador, descripcion, ciudadSalida, paisSalida, ciudadLlegada, paisLlegada, duracion, precio) VALUES ('Ea.', 'Fugit et.', 'New Gregoryl', 'GB', 'New Gregoryl', 'GB', 7, 6);
INSERT INTO Circuito (identificador, descripcion, ciudadSalida, paisSalida, ciudadLlegada, paisLlegada, duracion, precio) VALUES ('Est.', 'Qui quia.', 'Port Cameron', 'RU', 'Port Cameron', 'RU', 9, 1);
INSERT INTO Circuito (identificador, descripcion, ciudadSalida, paisSalida, ciudadLlegada, paisLlegada, duracion, precio) VALUES ('Et.', 'Eos quos.', 'East Dorisch', 'ES', 'East Dorisch', 'ES', 3, 9);
INSERT INTO Circuito (identificador, descripcion, ciudadSalida, paisSalida, ciudadLlegada, paisLlegada, duracion, precio) VALUES ('Ipsa.', 'Explicabo.', 'Wardmouth', 'US', 'Wardmouth', 'US', 7, 8);
INSERT INTO Circuito (identificador, descripcion, ciudadSalida, paisSalida, ciudadLlegada, paisLlegada, duracion, precio) VALUES ('Iste.', 'Sunt sit.', 'East Cristop', 'DE', 'East Cristop', 'DE', 3, 6);
INSERT INTO Circuito (identificador, descripcion, ciudadSalida, paisSalida, ciudadLlegada, paisLlegada, duracion, precio) VALUES ('Nemo.', 'Quis nisi.', 'North Pietro', 'IN', 'North Pietro', 'IN', 5, 1);
INSERT INTO Circuito (identificador, descripcion, ciudadSalida, paisSalida, ciudadLlegada, paisLlegada, duracion, precio) VALUES ('Non.', 'Et qui est.', 'Aureliamouth', 'CA', 'Aureliamouth', 'CA', 1, 9);
INSERT INTO Circuito (identificador, descripcion, ciudadSalida, paisSalida, ciudadLlegada, paisLlegada, duracion, precio) VALUES ('Odit.', 'Harum aut.', 'Aureliamouth', 'CA', 'Aureliamouth', 'CA', 7, 2);
INSERT INTO Circuito (identificador, descripcion, ciudadSalida, paisSalida, ciudadLlegada, paisLlegada, duracion, precio) VALUES ('Qui.', 'Sed odit.', 'Charlieport', 'CN', 'Charlieport', 'CN', 3, 6);
INSERT INTO Circuito (identificador, descripcion, ciudadSalida, paisSalida, ciudadLlegada, paisLlegada, duracion, precio) VALUES ('Quia.', 'Nobis et ut.', 'Hageneshaven', 'FR', 'Hageneshaven', 'FR', 8, 4);
INSERT INTO Circuito (identificador, descripcion, ciudadSalida, paisSalida, ciudadLlegada, paisLlegada, duracion, precio) VALUES ('Sit.', 'Occaecati.', 'Onieland', 'IT', 'Onieland', 'IT', 7, 6);
INSERT INTO Circuito (identificador, descripcion, ciudadSalida, paisSalida, ciudadLlegada, paisLlegada, duracion, precio) VALUES ('Unde.', 'Itaque.', 'Pinkview', 'MX', 'Pinkview', 'MX', 8, 3);
INSERT INTO Circuito (identificador, descripcion, ciudadSalida, paisSalida, ciudadLlegada, paisLlegada, duracion, precio) VALUES ('Ut.', 'Laboriosam.', 'New Selinala', 'IE', 'New Selinala', 'IE', 5, 2);


#
# TABLE STRUCTURE FOR: Ciudad
#

DROP TABLE IF EXISTS Ciudad;

CREATE TABLE Ciudad (
  pais char(12) COLLATE utf8_unicode_ci NOT NULL,
  nombre char(12) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (pais,nombre)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO Ciudad (pais, nombre) VALUES ('CA', 'New Gregoryl');
INSERT INTO Ciudad (pais, nombre) VALUES ('CN', 'North Pietro');
INSERT INTO Ciudad (pais, nombre) VALUES ('DE', 'New Selinala');
INSERT INTO Ciudad (pais, nombre) VALUES ('ES', 'Aureliamouth');
INSERT INTO Ciudad (pais, nombre) VALUES ('FR', 'Port Cameron');
INSERT INTO Ciudad (pais, nombre) VALUES ('GB', 'Hageneshaven');
INSERT INTO Ciudad (pais, nombre) VALUES ('IE', 'East Cristop');
INSERT INTO Ciudad (pais, nombre) VALUES ('IN', 'East Dorisch');
INSERT INTO Ciudad (pais, nombre) VALUES ('IT', 'Pinkview');
INSERT INTO Ciudad (pais, nombre) VALUES ('MX', 'Wardmouth');
INSERT INTO Ciudad (pais, nombre) VALUES ('PT', 'Charlieport');
INSERT INTO Ciudad (pais, nombre) VALUES ('RU', 'Onieland');
INSERT INTO Ciudad (pais, nombre) VALUES ('US', 'Port Alessan');


#
# TABLE STRUCTURE FOR: Etapa
#

DROP TABLE IF EXISTS Etapa;

CREATE TABLE Etapa (
  identificador char(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  orden int DEFAULT NULL,
  nombreLugar char(12) COLLATE utf8_unicode_ci DEFAULT NULL,
  ciudad char(12) COLLATE utf8_unicode_ci DEFAULT NULL,
  pais char(12) COLLATE utf8_unicode_ci DEFAULT NULL,
  duracion int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO Etapa (identificador, orden, nombreLugar, ciudad, pais, duracion) VALUES ('A.', 2, 'Breitenberg', 'Aureliamouth', 'CA', 6);
INSERT INTO Etapa (identificador, orden, nombreLugar, ciudad, pais, duracion) VALUES ('Aut.', 6, 'Nichole Way', 'Charlieport', 'CN', 6);
INSERT INTO Etapa (identificador, orden, nombreLugar, ciudad, pais, duracion) VALUES ('Ea.', 8, 'Homenick Loc', 'East Cristop', 'DE', 6);
INSERT INTO Etapa (identificador, orden, nombreLugar, ciudad, pais, duracion) VALUES ('Est.', 5, 'Kuhlman Prai', 'East Dorisch', 'ES', 9);
INSERT INTO Etapa (identificador, orden, nombreLugar, ciudad, pais, duracion) VALUES ('Et.', 9, 'Jett Mountai', 'Hageneshaven', 'FR', 6);
INSERT INTO Etapa (identificador, orden, nombreLugar, ciudad, pais, duracion) VALUES ('Ipsa.', 0, 'Zemlak Ferry', 'New Gregoryl', 'GB', 6);
INSERT INTO Etapa (identificador, orden, nombreLugar, ciudad, pais, duracion) VALUES ('Iste.', 3, 'Brielle Summ', 'New Selinala', 'IE', 1);
INSERT INTO Etapa (identificador, orden, nombreLugar, ciudad, pais, duracion) VALUES ('Nemo.', 7, 'Jakubowski K', 'North Pietro', 'IN', 4);
INSERT INTO Etapa (identificador, orden, nombreLugar, ciudad, pais, duracion) VALUES ('Non.', 1, 'Schoen Rue', 'Onieland', 'IT', 3);
INSERT INTO Etapa (identificador, orden, nombreLugar, ciudad, pais, duracion) VALUES ('Odit.', 4, 'Virgil Mills', 'Pinkview', 'MX', 0);


#
# TABLE STRUCTURE FOR: FechaCircuito
#

DROP TABLE IF EXISTS FechaCircuito;

CREATE TABLE FechaCircuito (
  fechaSalida date NOT NULL,
  identificador char(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  nbPersonas int DEFAULT NULL,
  PRIMARY KEY (fechaSalida)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO FechaCircuito (fechaSalida, identificador, nbPersonas) VALUES ('1970-02-02', 'Nemo.', 7);
INSERT INTO FechaCircuito (fechaSalida, identificador, nbPersonas) VALUES ('1977-11-14', 'Unde.', 4);
INSERT INTO FechaCircuito (fechaSalida, identificador, nbPersonas) VALUES ('1980-06-14', 'Et.', 7);
INSERT INTO FechaCircuito (fechaSalida, identificador, nbPersonas) VALUES ('1982-04-17', 'Aut.', 1);
INSERT INTO FechaCircuito (fechaSalida, identificador, nbPersonas) VALUES ('1989-02-10', 'Est.', 1);
INSERT INTO FechaCircuito (fechaSalida, identificador, nbPersonas) VALUES ('1990-10-09', 'Non.', 6);
INSERT INTO FechaCircuito (fechaSalida, identificador, nbPersonas) VALUES ('1991-07-03', 'Ut.', 5);
INSERT INTO FechaCircuito (fechaSalida, identificador, nbPersonas) VALUES ('1994-12-25', 'Sit.', 4);
INSERT INTO FechaCircuito (fechaSalida, identificador, nbPersonas) VALUES ('1996-06-04', 'Iste.', 2);
INSERT INTO FechaCircuito (fechaSalida, identificador, nbPersonas) VALUES ('1999-03-20', 'Ea.', 0);
INSERT INTO FechaCircuito (fechaSalida, identificador, nbPersonas) VALUES ('1999-08-18', 'Est.', 5);
INSERT INTO FechaCircuito (fechaSalida, identificador, nbPersonas) VALUES ('2000-04-17', 'Ea.', 6);
INSERT INTO FechaCircuito (fechaSalida, identificador, nbPersonas) VALUES ('2001-03-03', 'Qui.', 5);
INSERT INTO FechaCircuito (fechaSalida, identificador, nbPersonas) VALUES ('2003-05-20', 'Quia.', 4);
INSERT INTO FechaCircuito (fechaSalida, identificador, nbPersonas) VALUES ('2005-03-26', 'Ipsa.', 0);
INSERT INTO FechaCircuito (fechaSalida, identificador, nbPersonas) VALUES ('2007-05-12', 'A.', 4);
INSERT INTO FechaCircuito (fechaSalida, identificador, nbPersonas) VALUES ('2011-01-13', 'Aut.', 4);
INSERT INTO FechaCircuito (fechaSalida, identificador, nbPersonas) VALUES ('2011-03-18', 'Et.', 6);
INSERT INTO FechaCircuito (fechaSalida, identificador, nbPersonas) VALUES ('2016-11-30', 'Odit.', 1);
INSERT INTO FechaCircuito (fechaSalida, identificador, nbPersonas) VALUES ('2017-05-18', 'A.', 0);


#
# TABLE STRUCTURE FOR: Hotel
#

DROP TABLE IF EXISTS Hotel;

CREATE TABLE Hotel (
  direccion char(20) COLLATE utf8_unicode_ci NOT NULL,
  nombre char(12) COLLATE utf8_unicode_ci NOT NULL,
  ciudad char(12) COLLATE utf8_unicode_ci DEFAULT NULL,
  pais char(12) COLLATE utf8_unicode_ci DEFAULT NULL,
  numCuartos int DEFAULT NULL,
  precioCuarto int DEFAULT NULL,
  precioDesayuno int DEFAULT NULL,
  PRIMARY KEY (direccion,nombre)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO Hotel (direccion, nombre, ciudad, pais, numCuartos, precioCuarto, precioDesayuno) VALUES ('1025 Spencer Ways', 'Wunsch', 'Hageneshaven', 'FR', 6, 7, 6);
INSERT INTO Hotel (direccion, nombre, ciudad, pais, numCuartos, precioCuarto, precioDesayuno) VALUES ('1211 Reinger Gardens', 'Heller', 'Pinkview', 'MX', 2, 5, 9);
INSERT INTO Hotel (direccion, nombre, ciudad, pais, numCuartos, precioCuarto, precioDesayuno) VALUES ('136 Braxton Parkway', 'Becker', 'East Dorisch', 'ES', 5, 7, 4);
INSERT INTO Hotel (direccion, nombre, ciudad, pais, numCuartos, precioCuarto, precioDesayuno) VALUES ('19431 Flatley Road', 'Powlowski', 'North Pietro', 'IN', 4, 5, 1);
INSERT INTO Hotel (direccion, nombre, ciudad, pais, numCuartos, precioCuarto, precioDesayuno) VALUES ('19814 Oren Spurs Sui', 'Ernser', 'East Dorisch', 'ES', 8, 9, 3);
INSERT INTO Hotel (direccion, nombre, ciudad, pais, numCuartos, precioCuarto, precioDesayuno) VALUES ('2246 Thiel View', 'Schuppe', 'Aureliamouth', 'CA', 3, 7, 0);
INSERT INTO Hotel (direccion, nombre, ciudad, pais, numCuartos, precioCuarto, precioDesayuno) VALUES ('2435 Renner Greens', 'White', 'New Gregoryl', 'GB', 7, 2, 7);
INSERT INTO Hotel (direccion, nombre, ciudad, pais, numCuartos, precioCuarto, precioDesayuno) VALUES ('246 Germaine Ports S', 'Wilderman', 'Aureliamouth', 'CA', 1, 5, 5);
INSERT INTO Hotel (direccion, nombre, ciudad, pais, numCuartos, precioCuarto, precioDesayuno) VALUES ('2474 Quinn Mountains', 'Gaylord', 'Pinkview', 'MX', 2, 7, 2);
INSERT INTO Hotel (direccion, nombre, ciudad, pais, numCuartos, precioCuarto, precioDesayuno) VALUES ('2584 Rohan Burgs Sui', 'Breitenberg', 'Onieland', 'IT', 0, 9, 9);
INSERT INTO Hotel (direccion, nombre, ciudad, pais, numCuartos, precioCuarto, precioDesayuno) VALUES ('26033 Klocko Straven', 'Welch', 'New Selinala', 'IE', 8, 9, 9);
INSERT INTO Hotel (direccion, nombre, ciudad, pais, numCuartos, precioCuarto, precioDesayuno) VALUES ('2835 Ledner Bypass S', 'Ebert', 'East Cristop', 'DE', 6, 6, 5);
INSERT INTO Hotel (direccion, nombre, ciudad, pais, numCuartos, precioCuarto, precioDesayuno) VALUES ('33548 Krajcik Greens', 'Bode', 'New Gregoryl', 'GB', 8, 6, 7);
INSERT INTO Hotel (direccion, nombre, ciudad, pais, numCuartos, precioCuarto, precioDesayuno) VALUES ('3472 Clemmie Motorwa', 'Schmidt', 'Charlieport', 'CN', 2, 4, 6);
INSERT INTO Hotel (direccion, nombre, ciudad, pais, numCuartos, precioCuarto, precioDesayuno) VALUES ('354 Kohler Crossroad', 'Robel', 'New Selinala', 'IE', 6, 4, 0);
INSERT INTO Hotel (direccion, nombre, ciudad, pais, numCuartos, precioCuarto, precioDesayuno) VALUES ('38119 Litzy Junction', 'Wisozk', 'East Dorisch', 'ES', 7, 8, 2);
INSERT INTO Hotel (direccion, nombre, ciudad, pais, numCuartos, precioCuarto, precioDesayuno) VALUES ('47652 Dietrich Loop', 'Tromp', 'Charlieport', 'CN', 0, 7, 9);
INSERT INTO Hotel (direccion, nombre, ciudad, pais, numCuartos, precioCuarto, precioDesayuno) VALUES ('656 Gaylord Branch S', 'Ullrich', 'Onieland', 'IT', 0, 4, 8);
INSERT INTO Hotel (direccion, nombre, ciudad, pais, numCuartos, precioCuarto, precioDesayuno) VALUES ('67569 Shields Valley', 'Gorczany', 'Aureliamouth', 'CA', 4, 0, 9);
INSERT INTO Hotel (direccion, nombre, ciudad, pais, numCuartos, precioCuarto, precioDesayuno) VALUES ('699 Laura Neck Suite', 'Franecki', 'Hageneshaven', 'FR', 9, 7, 0);
INSERT INTO Hotel (direccion, nombre, ciudad, pais, numCuartos, precioCuarto, precioDesayuno) VALUES ('7029 McGlynn Tunnel', 'Bins', 'East Cristop', 'DE', 5, 9, 5);
INSERT INTO Hotel (direccion, nombre, ciudad, pais, numCuartos, precioCuarto, precioDesayuno) VALUES ('7265 Thiel Avenue', 'Pacocha', 'Hageneshaven', 'FR', 9, 5, 4);
INSERT INTO Hotel (direccion, nombre, ciudad, pais, numCuartos, precioCuarto, precioDesayuno) VALUES ('84608 Corrine Run Su', 'Stroman', 'East Cristop', 'DE', 6, 7, 0);
INSERT INTO Hotel (direccion, nombre, ciudad, pais, numCuartos, precioCuarto, precioDesayuno) VALUES ('8587 Derick Light', 'Treutel', 'Charlieport', 'CN', 7, 3, 6);
INSERT INTO Hotel (direccion, nombre, ciudad, pais, numCuartos, precioCuarto, precioDesayuno) VALUES ('99610 Stoltenberg Cu', 'Smitham', 'North Pietro', 'IN', 7, 0, 1);


#
# TABLE STRUCTURE FOR: LugarAvisitar
#

DROP TABLE IF EXISTS LugarAvisitar;

CREATE TABLE LugarAvisitar (
  ciudad char(12) COLLATE utf8_unicode_ci DEFAULT NULL,
  nombre char(12) COLLATE utf8_unicode_ci NOT NULL,
  pais char(12) COLLATE utf8_unicode_ci DEFAULT NULL,
  direccion char(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  descripccion char(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  precio int DEFAULT NULL,
  PRIMARY KEY (nombre)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO LugarAvisitar (ciudad, nombre, pais, direccion, descripccion, precio) VALUES ('Aureliamouth', 'Breitenberg', 'CA', '585 Jayce Tunnel Apt', 'Eius culpa ut aut nisi iste.', 4);
INSERT INTO LugarAvisitar (ciudad, nombre, pais, direccion, descripccion, precio) VALUES ('Charlieport', 'Brielle Summ', 'CN', '369 Simeon Shores Ap', 'Ipsa ea eaque repudiandae ipsum.', 8);
INSERT INTO LugarAvisitar (ciudad, nombre, pais, direccion, descripccion, precio) VALUES ('East Cristop', 'Homenick Loc', 'DE', '58293 Tillman Divide', 'Voluptatum et eum consequatur nostrum.', 4);
INSERT INTO LugarAvisitar (ciudad, nombre, pais, direccion, descripccion, precio) VALUES ('East Dorisch', 'Jakubowski K', 'ES', '0723 Medhurst Manors', 'Qui autem sed ad voluptas ad nam quia.', 6);
INSERT INTO LugarAvisitar (ciudad, nombre, pais, direccion, descripccion, precio) VALUES ('Hageneshaven', 'Jett Mountai', 'FR', '97784 Jenkins Vista', 'Delectus eum eum sed natus.', 1);
INSERT INTO LugarAvisitar (ciudad, nombre, pais, direccion, descripccion, precio) VALUES ('New Gregoryl', 'Kuhlman Prai', 'GB', '0917 Reyna Port Suit', 'Quo nam illum fuga fugit sint nihil.', 0);
INSERT INTO LugarAvisitar (ciudad, nombre, pais, direccion, descripccion, precio) VALUES ('New Selinala', 'Nichole Way', 'IE', '532 Justice Place', 'Fugit optio quia velit nostrum.', 7);
INSERT INTO LugarAvisitar (ciudad, nombre, pais, direccion, descripccion, precio) VALUES ('North Pietro', 'Schoen Rue', 'IN', '931 Ed Tunnel Apt. 7', 'Voluptate quia distinctio aperiam.', 0);
INSERT INTO LugarAvisitar (ciudad, nombre, pais, direccion, descripccion, precio) VALUES ('Onieland', 'Virgil Mills', 'IT', '7627 Schimmel Brooks', 'Et quia beatae fuga beatae.', 0);
INSERT INTO LugarAvisitar (ciudad, nombre, pais, direccion, descripccion, precio) VALUES ('Pinkview', 'Zemlak Ferry', 'MX', '74549 Homenick Valle', 'Nemo corporis corporis optio mollitia.', 4);


#
# TABLE STRUCTURE FOR: Simulacion
#

DROP TABLE IF EXISTS Simulacion;

CREATE TABLE Simulacion (
  idSim int NOT NULL,
  userName char(12) COLLATE utf8_unicode_ci DEFAULT NULL,
  pais char(12) COLLATE utf8_unicode_ci DEFAULT NULL,
  ciudad char(12) COLLATE utf8_unicode_ci DEFAULT NULL,
  id char(12) COLLATE utf8_unicode_ci DEFAULT NULL,
  fechaSalida date DEFAULT NULL,
  noPerson int DEFAULT NULL,
  nameHotel char(12) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (idSim)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

#
# TABLE STRUCTURE FOR: User
#

DROP TABLE IF EXISTS User;

CREATE TABLE User (
  userName char(12) COLLATE utf8_unicode_ci NOT NULL,
  email char(12) COLLATE utf8_unicode_ci DEFAULT NULL,
  number char(12) COLLATE utf8_unicode_ci DEFAULT NULL,
  address char(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  tipoDePago char(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (userName)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
