


fun main(args: Array<String>){

    val chooseUIOpts =listOf<Option>(

        Option("Exit") { -1 },

        Option("Brochure") {
            // TODO: Interface 1
            0
        },

        Option("Generate Simulation") {
            generateSimulation()
            0
        },

        Option("View a Simulation") {
            println("Enter a simulation id")
            var input = readLine()?.toInt() ?: 0
            print(DB().getSimulation(input))
            0
        },

        Option("Validate Simulation and make Reservation") {
            println("Enter a simulation id")
            var input = readLine()?.toInt() ?: 0
            DB().validateSimulation(input)
            0
        }

    )

    val chooseUI = UI("""Welcome to Travel agency Ponchito""", """Goodbye""", chooseUIOpts, """Choose an action""")
    chooseUI.interactiveLoop()

}

