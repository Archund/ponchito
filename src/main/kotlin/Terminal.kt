import kotlin.math.max
import kotlin.math.min

const val ESC = "\u001b"

fun getTermWidth(): Int? {
    val widthProcess = ProcessBuilder("tput", "cols").start()
    var width: Int = 0
    widthProcess.inputStream.reader(Charsets.UTF_8).use {
        print(it.readText().trimEnd())
        width = 0//TODO: it.readText().trimEnd().toInt()
    }

    return null
}

val colors = mapOf(
    // Clear
    0   to  "0m",   "clear"     to  "0m",
    // Foreground colors
    1   to  "30m",  "default"   to  "30m",
    2   to  "31m",  "red"       to  "31m",
    3   to  "32m",  "green"     to  "32m",
    4   to  "33m",  "yellow"    to  "33m",
    5   to  "34m",  "blue"      to  "34m",
    6   to  "35m",  "purple"    to  "35m",
    7   to  "36m",  "cyan"      to  "36m",
    // Background colors
    -1  to  "40m",  "bDefault"  to  "40m",
    -2  to  "41m",  "bRed"      to  "41m",
    -3  to  "42m",  "bGreen"    to  "42m",
    -4  to  "43m",  "bYellow"   to  "43m",
    -5  to  "44m",  "bBlue"     to  "44m",
    -6  to  "45m",  "bPurple"   to  "45m",
    -7  to  "46m",  "bCyan"     to  "46m",
    //Control
    "bold"      to  "1m",   "faint"         to  "2m",   "intensityOff"  to  "22m",
    "italic"    to  "3m",   "fraktur"       to  "20m",  "italicOff"     to  "23m",
    "underline" to  "4m",   "underlineOff"  to  "24m",
    "blink"     to  "5m",   "fastBlink"     to  "6m",   "blinkOff"      to  "25m",
    "inverted"  to  "7m",   "invertedOff"   to  "27m",
    "conceal"   to  "8m",   "concealOff"    to  "28m",
    "crossed"   to  "9m",   "crossedOff"    to  "29m"
)

fun term(color:      Any = 0,
         bold:       Any = "",
         faint:      Any = "",
         inverted:   Any = "",
         italic:     Any = "",
         fraktur:    Any = "",
         underline:  Any = "",
         blink:      Any = "",
         fastBlink:  Any = "",
         conceal:    Any = "",
         crossed:    Any = "",
         custom:     String = ""
)                       {
    print(color(color, bold, faint, inverted, italic, fraktur, underline, blink, fastBlink, conceal, crossed, custom))
}

fun color(color:      Any = 0,
          bold:       Any = "",
          faint:      Any = "",
          inverted:   Any = "",
          italic:     Any = "",
          fraktur:    Any = "",
          underline:  Any = "",
          blink:      Any = "",
          fastBlink:  Any = "",
          conceal:    Any = "",
          crossed:    Any = "",
          custom:     String = ""
): String             {

    if (custom != "" ) return "$ESC[$custom"

    var output = ""

    output += when (color) {
        is Int, String  ->  "$ESC[${colors[color]}"
        else            ->  ""
    }

    output += when (bold) {
        is Boolean -> "$ESC[${if(bold) "${colors["bold"]}" else "${colors["intensityOff"]}" }"
        else -> ""
    }
    output += when (faint) {
        is Boolean -> "$ESC[${if(faint) "${colors["faint"]}" else "${colors["intensityOff"]}" }"
        else -> ""
    }
    output += when (inverted) {
        is Boolean -> "$ESC[${if(inverted) "${colors["inverted"]}" else "${colors["invertedOff"]}" }"
        else -> ""
    }
    output += when (italic) {
        is Boolean -> "$ESC[${if(italic) "${colors["italic"]}" else "${colors["italicOff"]}" }"
        else -> ""
    }
    output += when (fraktur) {
        is Boolean -> "$ESC[${if(fraktur) "${colors["fraktur"]}" else "${colors["italicOff"]}" }"
        else -> ""
    }
    output += when (underline) {
        is Boolean -> "$ESC[${if(underline) "${colors["underline"]}" else "${colors["underlineOff"]}" }"
        else -> ""
    }
    output += when (blink) {
        is Boolean -> "$ESC[${if(blink) "${colors["blink"]}" else "${colors["blinkOff"]}" }"
        else -> ""
    }
    output += when (fastBlink) {
        is Boolean -> "$ESC[${if(fastBlink) "${colors["fastBlink"]}" else "${colors["blinkOff"]}" }"
        else -> ""
    }
    output += when (conceal) {
        is Boolean -> "$ESC[${if(conceal) "${colors["conceal"]}" else "${colors["concealOff"]}" }"
        else -> ""
    }
    output += when (crossed) {
        is Boolean -> "$ESC[${if(crossed) "${colors["crossed"]}" else "${colors["crossedOff"]}" }"
        else -> ""
    }

    return output
}


fun box(    text:           String = "",
            padding:        Int = 3,
            margin:         Int = 4,
            border:         Int = 1,
            centerContent:  Boolean = false,
            centerBox:      Boolean = false
)                                    {

    //val termHeight = System.getenv("LINES") ?: 400
    //val termWidth: Int = (System.getenv("COLUMNS") ?: 320 ).toString().toInt()
    val termWidth = getTermWidth() ?: 320

    val lines = text.lines()
    val maxLength = lines.maxBy{it.length}!!.length + border*2 + padding*2

    var outPad = margin
    if (centerBox) outPad = max(margin, (termWidth-maxLength+2)/2)

    val superiorBorder = "${" ".repeat(outPad)}╭${"─".repeat(maxLength)}╮\n"
    val inferiorBorder = "${" ".repeat(outPad)}╰${"─".repeat(maxLength)}╯\n"




    print("\n$superiorBorder")
    lines.forEach {
        val totalPadding = maxLength - it.length - border*2
        val pad = totalPadding / 2
        if (centerContent)
            print("${" ".repeat(outPad)}│${" ".repeat(border)}${" ".repeat(pad)}$it${" ".repeat(pad)}${" ".repeat(border)}│\n")
        else
            print("${" ".repeat(outPad)}│${" ".repeat(border)}$it${" ".repeat(totalPadding)}${" ".repeat(border)}│\n")
    }
    print("$inferiorBorder\n")
}

fun notification(text:String) {
    box(text)
}

fun banner(text:String) {
    box(text= text, centerContent= true, centerBox= true, padding = ( (getTermWidth()?:320) *.25).toInt() )
}


fun clear() {
    // TODO: Clear term
    print("$ESC[0m")
}
