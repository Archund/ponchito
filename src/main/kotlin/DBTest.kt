import com.zaxxer.hikari.HikariConfig
import com.zaxxer.hikari.HikariDataSource
import javax.sql.DataSource
import kotliquery.*



/*
session.run(queryOf("""
  create table members (
    id serial not null primary key,
    name varchar(64),
    created_at timestamp not null
  )
""").asExecute) // returns Boolean
 */

/*
 UPDATE

val insertQuery: String = "insert into members (name,  created_at) values (?, ?)"

session.run(queryOf(insertQuery, "Alice", Date()).asUpdate) // returns effected row count
session.run(queryOf(insertQuery, "Bob", Date()).asUpdate)


 */

fun main(args: Array<String>) {
    //jdbc:postgresql://server.local:5432/myDatabase
    //val session = sessionOf("jdbc:postgresql://localhost:5555/ponchito", "arc", "")
    //val session = MyDB.getSession()

    //val allMembersQuery = queryOf("select nombre, pais from 'Ciudad''").map(toCiudad).asList
    //val cities: List<Ciudad> = session.run(allMembersQuery)

    HikariCP.default("jdbc:postgresql://localhost:5555/ponchito", "postgres", "postgres")

    using(sessionOf(HikariCP.dataSource())) { session ->
        /*session.run(queryOf("""
                                        |create table members (
                                        |id serial not null primary key,
                                        |name varchar(64),
                                        |created_at timestamp not null
                                        |)
                                        """.trimIndent().trimMargin()).asExecute) // returns Boolean
         */
        val allMembersQuery = queryOf("select * from Ciudad").map(toCiudad).asList
        val cities: List<Ciudad> = session.run(allMembersQuery)
        cities.forEach {
            print("Element: ${it.nombre}| ${it.pais}|")
        }


        //val aliceQuery = queryOf("select nombre, pais from 'Ciudad'", "Alice").map(toCiudad).asSingle
        //val alice: Ciudad? = session.run(aliceQuery)
        //val kk = queryOf()


    }





}


class MyDB {
    companion object {
        var url: String = "jdbc:postgresql://server.local:5432/ponchito"
        var user: String = "postgres"
        var pass: String = "postgres"

        val config: HikariConfig = HikariConfig()

        private val dataSource by lazy {
            var hikariConfig: HikariConfig =  HikariConfig();
            hikariConfig.setDriverClassName("org.postgresql.Driver");
            hikariConfig.setJdbcUrl(url);
            hikariConfig.setUsername(user);
            hikariConfig.setPassword(pass);

            hikariConfig.setMaximumPoolSize(5);
            hikariConfig.setConnectionTestQuery("SELECT 1");
            hikariConfig.setPoolName("springHikariCP");

            /*
            hikariConfig.addDataSourceProperty("dataSource.cachePrepStmts", "true");
            hikariConfig.addDataSourceProperty("dataSource.prepStmtCacheSize", "250");
            hikariConfig.addDataSourceProperty("dataSource.prepStmtCacheSqlLimit", "2048");
            hikariConfig.addDataSourceProperty("dataSource.useServerPrepStmts", "true");
             */

            var dataSource: HikariDataSource  = HikariDataSource(hikariConfig);

            dataSource;
        }

        @JvmStatic fun getSession(): Session {
            return sessionOf(dataSource)
        }
    }
}

data class User(val id:Int, val username: String, val usertype: String)

class UserDAO {

    val toUser: (Row) -> User = { row ->
        User(
            row.int("id"),
            row.string("username"),
            row.string("usertype")
        )
    }

    fun getAllUsers(): List<User> {
        var returnedList: List<User> = arrayOf<User>().toList()
        using(MyDB.getSession()) { session ->

            val allUsersQuery = queryOf("select * from quintor_user").map(toUser).asList
            returnedList = session.run(allUsersQuery)
            session.connection.close()
            session.close()
        }

        return returnedList
    }
}