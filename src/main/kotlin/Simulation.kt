import java.sql.Date

fun generateSimulation() {


    var userName = ""
    println("Enter a User name")
    userName = readLine() ?: "nope"

    val tmp = DB().getUser(userName)
    //println( tmp)

    val registeredUser = tmp != 0


    val countries = DB().getCountries()//listOf("France", "Morocco", "Belarus", "Germany")
    //print(countries)
    var myCountry:String = ""



    val chooseCountryOpts = mutableListOf<Option>(
        Option("Cancel") { -1 }
    )
    for(country in countries){
        chooseCountryOpts.add(Option(country){
            myCountry = country
            println("Selected $country")
            -1
        })
    }
    var chooseCountry = UI("""Select a Country""", """""", chooseCountryOpts, """Choose an Country""")

    chooseCountry.interactiveLoop()

    val circuitsInCountry = DB().getCircuits(myCountry)
    var myCircuit: Circuito = Circuito("","","","","","",0,0)

    val chooseCircuitOpts = mutableListOf<Option>(
        Option("Cancel") { -1 }
    )
    for(circuit in circuitsInCountry){
        chooseCircuitOpts.add(Option(circuit.toString()){2
            myCircuit = circuit
            println("Selected ${myCircuit.toString()}")
            -1
        })
    }

    val chooseCircuit = UI("""Select a City""", """""", chooseCircuitOpts, """Choose a City""")
    chooseCircuit.interactiveLoop()
    //println("Captured City value is $myCircuit")


    val datesOfCircuit = DB().getCircuitDates(myCircuit.identificador)
    var myDate: FechaCircuito = FechaCircuito(Date(100),"",0)

    val chooseDateOpts = mutableListOf<Option>(
        Option("Cancel") { -1 }
    )
    for(date in datesOfCircuit){
        chooseDateOpts.add(Option(date.toString()){
            myDate = date
            println("Selected ${myDate.toString()}")
            -1
        })
    }
    val chooseDate = UI("""Select a Date""", "", chooseDateOpts, """Choose a Date""")
    chooseDate.interactiveLoop()

    val hotelsOfCircuit = DB().getHotels(myCircuit.paisLlegada,myCircuit.ciudadLlegada)
    var myHotel: Hotel = Hotel("","","",0,0,0)

    val chooseHotelOpts = mutableListOf<Option>(
        Option("Cancel") { -1 }
    )
    for(hotel in hotelsOfCircuit){
        chooseHotelOpts.add(Option(hotel.toString()){
            myHotel = hotel
            println("Selected ${myHotel.toString()}")
            -1
        })
    }

    val chooseHotel = UI("""Select a Hotel""", "", chooseHotelOpts, """Choose a Hotel""")
    chooseHotel.interactiveLoop()

    println("Enter number of travelers")
    val travelers:Int = readLine()?.toInt() ?: 1

    DB().pushSimulation(Simulacion(0, userName, myCountry, myCircuit.ciudadLlegada, myCircuit.identificador, myDate.fechaSalida, travelers, myHotel.nombre))


    println("Final Selection :\n\tUsername: $userName \t Registered: $registeredUser\n\tCountry: $myCountry\n\tCircuit: $myCircuit\n\tDate: $myDate\n\tHotel: $myHotel")

}