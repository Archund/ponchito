// Define TypeAlias for Option : ID, Description, Function
data class OptionClass(val description: String, val func: () -> Int)
typealias Option = OptionClass
//typealias Option = Pair<String, () -> Int>

// List of my options, includes exit command
class UI(welcome: String, goodbye: String, opts: List<Option>, optionMessage: String) {

    private var availableOptions: List<Option> = opts
    private val welcomeMessage: String = welcome
    private val goodbyeMessage: String = goodbye
    private val message: String

    init {
        message = optionMessageBuilder(optionMessage)
    }

    private fun optionMessageBuilder(optionMessage: String): String {

        var message =   """
                    |
                    |"""

        message += optionMessage
        message += """
                |"""

        availableOptions.forEachIndexed { index, opt ->
            message +=  """
                    |   $index) ${opt.description}
                    """
        }
        message += """|"""

        return message.trimIndent().trimMargin()
    }

    fun interactiveLoop() {

        // Print Welcome
        term(4)
        print("$welcomeMessage\n")
        term(0)

        var running = true
        while (running) {

            term(6)
            print("$message\n")
            term(0)
            // Read & validate user input
            var input = readLine()?.toIntOrNull()
            while (input == null || input !in 0 until availableOptions.count()) {
                term(2)
                box(text= "Invalid Option", border= 1, margin= 3, centerContent= true)
                term(0)

                term(6)
                print("$message\n")
                term(0)
                input = readLine()?.toIntOrNull()
            }

            when(val result = availableOptions[input].func()) {
                -1 -> running = false // Exit code
            }

        }

        // Print Goodbye
        term(4)
        print("$goodbyeMessage\n")
        term(0)
    }
}

