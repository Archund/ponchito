import com.zaxxer.hikari.HikariConfig
import com.zaxxer.hikari.HikariDataSource
import kotliquery.*



class DB() {

    private val database = HikariCP.default("jdbc:postgresql://localhost:5555/ponchito", "postgres", "postgres")
    private val session = sessionOf(database)

    fun query(q: String){
            /*session.run(queryOf("""
                                            |create table members (
                                            |id serial not null primary key,
                                            |name varchar(64),
                                            |created_at timestamp not null
                                            |)
                                            """.trimIndent().trimMargin()).asExecute) // returns Boolean
             */
            val allMembersQuery = queryOf("select * from Ciudad").map(toCiudad).asList
            val cities: List<Ciudad> = session.run(allMembersQuery)
            cities.forEach {
                print("Element: ${it.nombre}| ${it.pais}|")
            }

        val query = queryOf(q)

        //return session.run(query)
    }

    fun getUser(u: String): Int {
        var queryString=   """
                            |select count(userName) as count
                            |from Users
                            |where userName = ?""".trimIndent().trimMargin()

        val query = queryOf(queryString,u).map { row -> row.string("count") }.asSingle

        val res = (session.run(query) ?: 0).toString().toInt()
        return res
    }

    fun getCountries():List<String> {

        val countryList = mutableListOf<String>()

        val queryString =   """
                            |select pais from Ciudad
                            """.trimIndent().trimMargin()

        val query = queryOf(queryString).map { row -> row.string("pais") }.asList

        countryList.addAll(session.run(query))

        return countryList
    }

    fun getCircuits(inCountry: String): List<Circuito> {

        val circuitList = mutableListOf<Circuito>()

        var queryString =   """
                            |select *
                            |from Circuito
                            |where paisSalida = ? and paisLlegada = ?""".trimIndent().trimMargin()

        val query = queryOf(queryString,inCountry,inCountry).map(toCircuito).asList

        circuitList.addAll(session.run(query))
        return circuitList
    }

    fun getCircuitDates(ofCircuit:String): List<FechaCircuito> {
        val dateList = mutableListOf<FechaCircuito>()

        var queryString =   """
                            |select *
                            |from FechaCircuito
                            |where identificador = ?""".trimIndent().trimMargin()

        val query = queryOf(queryString,ofCircuit).map(toFechaCircuito).asList

        dateList.addAll(session.run(query))
        return dateList
    }

    fun getHotels(paisLlegada: String, ciudadLlegada: String): List<Hotel> {
        val hotelList = mutableListOf<Hotel>()

        var queryString =   """
                            |select *
                            |from Hotel
                            |where ciudad = ?""".trimIndent().trimMargin()

        val query = queryOf(queryString,ciudadLlegada).map(toHotel).asList

        hotelList.addAll(session.run(query))
        return hotelList
    }

    fun pushSimulation(sim: Simulacion) {
        val insertQuery =   """
                            |insert into Simulacion ( userName, pais, ciudad, id, fechaSalida, noPerson, nameHotel )
                            |values ( ?, ?, ?, ?, ?, ?, ?)
                            """.trimIndent().trimMargin()

        val insert = queryOf(insertQuery, sim.userName, sim.pais, sim.ciudad, sim.id, sim.fechaSalida, sim.noPerson, sim.nameHotel)

        session.run(insert.asUpdate)
    }

    fun getSimulation(simID:Int): Simulacion? {
        val queryString =   """
                            |select * from Simulacion
                            |where idsim = ?
                            """.trimIndent().trimMargin()

        val query = queryOf(queryString,simID).map(toSimulacion).asSingle

        return session.run(query)


    }

    fun validateSimulation(sim: Int) {

    }


}