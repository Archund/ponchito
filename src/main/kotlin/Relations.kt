import java.sql.Date
import kotliquery.*

data class Circuito(
    val identificador:      String, // 05
    val descripcion:        String, // 12
    val ciudadSalida:       String, // 12
    val paisSalida:         String, // 12
    val ciudadLlegada:      String, // 12
    val paisLlegada:        String, // 12
    val duracion:           Int,
    val precio:             Int
)
val toCircuito: (Row) -> Circuito = { row ->
    Circuito(
        row.string("identificador"),
        row.string("descripcion"),
        row.string("ciudadSalida"),
        row.string("paisSalida"),
        row.string("ciudadLlegada"),
        row.string("paisLlegada"),
        row.int("duracion"),
        row.int("precio")
    )
}

data class FechaCircuito(
    val fechaSalida:        Date,
    val identificador:      String, // 12
    val nbPersonas:         Int
)
val toFechaCircuito: (Row) -> FechaCircuito = { row ->
    FechaCircuito(
        row.sqlDate("fechaSalida"),
        row.string("identificador"),
        row.int("nbPersonas")
    )
}


data class Ciudad(
    val pais:               String, // 12
    val nombre:             String  // 12
)
val toCiudad: (Row) -> Ciudad = { row ->
    Ciudad(
        row.string("pais"),
        row.string("nombre")
    )
}


data class Etapa(
    val identificador:      String, // 05
    val orden:              Int,
    val nombreLugar:        String, // 12
    val ciudad:             String, // 12
    val pais:               String, // 12
    val duracion:           Int
)
val toEtapa: (Row) -> Etapa = { row ->
    Etapa(
        row.string("identificador"),
        row.int("orden"),
        row.string("nombreLugar"),
        row.string("ciudad"),
        row.string("pais"),
        row.int("duracion")
    )
}


data class LugarAvisitar(
    val ciudad:             String, // 12
    val nombre:             String, // 12
    val pais:               String, // 12
    val direccion:          String, // 20
    val descripcion:        String, // 40
    val precio:             Int
)
val toLugarAvisitar: (Row) -> LugarAvisitar = { row ->
    LugarAvisitar(
        row.string("ciudad"),
        row.string("nombre"),
        row.string("pais"),
        row.string("direccion"),
        row.string("descripcion"),
        row.int("precio")
    )
}


data class Hotel(
    val direccion:          String, // 20
    val nombre:             String, // 12
    val ciudad:             String, // 12
    val numCuartos:         Int,
    val precioCuarto:       Int,
    val precioDesayuno:     Int
)
val toHotel: (Row) -> Hotel = { row ->
    Hotel(
        row.string("direccion"),
        row.string("nombre"),
        row.string("ciudad"),
        row.int("numCuartos"),
        row.int("precioCuarto"),
        row.int("precioDesayuno")
    )
}

data class Simulacion(
    val idSim:              Int,
    val userName:           String,
    val pais:               String,
    val ciudad:             String,
    val id:                 String,
    val fechaSalida:        Date,
    val noPerson:           Int,
    val nameHotel:          String
)
val toSimulacion: (Row) -> Simulacion = { row ->
    Simulacion(
            row.int("idSim"),
            row.string("userName"),
            row.string("pais"),
            row.string("ciudad"),
            row.string("id"),
            row.sqlDate("fechaSalida"),
            row.int("noPerson"),
            row.string("nameHotel")
        )
}
